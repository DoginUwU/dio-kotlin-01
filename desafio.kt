enum class Nivel { BASICO, INTERMEDIARIO, DIFICIL }

class Usuario(val nome: String, val idade: Int = 18) {
     override fun toString(): String {
        return nome
    }
}

data class ConteudoEducacional(val nome: String, val nivel: Nivel, val duracao: Int = 60)

data class Formacao(val nome: String, var conteudos: List<ConteudoEducacional>) {

    val inscritos = mutableListOf<Usuario>()
    
    fun matricular(usuario: Usuario) {
        inscritos.add(usuario)
    }
    
    override fun toString(): String {
        return "O curso de Formação Web tem os seguintes alunos matriculados: ${inscritos.toString()}"
    }
}

fun main() {
    val conteudoWeb1 = ConteudoEducacional("Javascript Lógica Inicial", Nivel.BASICO, 120)
    val conteudoWeb2 = ConteudoEducacional("Javascript Funções Assíncronas", Nivel.INTERMEDIARIO, 52)
    val conteudoWeb3 = ConteudoEducacional("Typescript e Conteúdos Genércios", Nivel.DIFICIL)
    
    val formacaoWeb = Formacao("Formação Web", listOf(conteudoWeb1, conteudoWeb2, conteudoWeb3))
    
    val usario1 = Usuario("Luiz", 20)
    val usario2 = Usuario("Ricardo")
    
    formacaoWeb.matricular(usario1)
    formacaoWeb.matricular(usario2)
    
    println(formacaoWeb)
}
